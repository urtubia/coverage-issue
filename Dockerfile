FROM python:3.8-buster as app
WORKDIR /code
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD coverage run --source=. -m unittest discover -s "test" -p "test_*.py" && coverage report
