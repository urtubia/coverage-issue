import unittest
import main

class TestHello(unittest.TestCase):

    def test_hello(self):
        h = main.say_hello()
        self.assertEqual(h, "hello")
